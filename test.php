<?php

$tester->new_session = [
  'tests' => [],
  'success' => 0,
  'fail' => 0,
  'total' => 0,
];

$tester->session = $tester->new_session;
$tester->session_echo = true; // Set false for API use

$tester->start = function() use ($tester) {
  $tester->session = $tester->new_session;
};

$tester->test = function($title, $fn) use ($tester) {

  $test_session = &$tester->session;
  $test_result = [
    'title' => $title,
    'assertions' => [],
    'success' => true,
    'error' => null,
  ];

  try {

    if (!is_callable($fn)) throw new Exception('$tester->test() expects a function as second argument', 1);

    $fn(function($assertion_title, $success = false) use (&$test_result) {

      $is_success = $success === true;

      if (!$is_success) {
        // All assertions must be true
        $test_result['success'] = false;
      }

      $test_result['assertions'] []= [
        'title' => $assertion_title,
        'success' => $is_success,
      ];
    });

  } catch (\Throwable $th) {
    $test_result['success'] = false;
    $test_result['error'] = $th->getMessage().' in '.str_replace(ABSPATH, '', $th->getFile()).' on line '.$th->getLine();
  }

  $test_session['tests'] []= $test_result;

  if ($test_result['success']) {
    $test_session['success']++;
  } else {
    $test_session['fail']++;
  }

  $test_session['total']++;
};

$tester->end = function() use ($tester) {
  $tester->session = $tester->new_session;
};
