# Tester Module

## Examples

Server-side

```php
$tester->test('Plugin function', function($it) {

  $it('Returns valid user ID', is_numeric( some_function() ));

});
```

### Useful PHP functions for assertions

```
empty
is_​array
is_​bool
is_​callable
is_​countable
is_​double
is_​float
is_​int
is_​integer
is_​iterable
is_​long
is_​null
is_​numeric
is_​object
is_​real
is_​resource
is_​scalar
is_​string
isset
```


```

Client-side

```js
test('Plugin function', function(it) {

  it('Returns a string', typeof someFunction() === 'string')

})
```
