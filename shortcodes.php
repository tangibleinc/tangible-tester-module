<?php

add_shortcode('ttester', function($atts, $content) use ($tester) {

  $previous_tests_path = $tester->get_tests_path();

  $route = isset($atts[0]) ? $atts[0] :
    (isset($atts['route']) ? $atts['route'] : '')
  ;

  $route_parts = explode('/', $route);
  $plugin_name = array_shift($route_parts);

  $tests_path = WP_CONTENT_DIR.'/plugins/'.$plugin_name.'/tests';

  $tester->set_tests_path( $tests_path );

  $current_route = implode('/', $route_parts);

  ob_start();
  $tester->start();
  $tester->load_test_path('index', $current_route);
  $tests_output = ob_get_clean();

  // Create test report
  ob_start();
  $tester->render_report();
  $report = ob_get_clean();

  $tester->end();

  // Restore tests path
  $tester->set_tests_path( $previous_tests_path );

  return (!empty($report) ? $report.'<hr>' : '').$tests_output;
});
