<?php

require_once __DIR__.'/tangible-module.php';

if (!function_exists('tangible_tester')) {

  function tangible_tester($instance = false) {
    static $o;
    if (is_a($instance, 'TangibleModule')) {
      $o = $instance->latest;
    }
    return $o;
  }
}

return tangible_tester(new class extends TangibleModule {

  public $name = 'tangible_tester';
  public $version = '20191225';

  function __construct() {
    parent::__construct();
  }

  function load_latest_version() {

    $tester = $this;

    require_once __DIR__.'/paths.php';
    require_once __DIR__.'/report.php';
    require_once __DIR__.'/shortcodes.php';
    require_once __DIR__.'/test.php';
  }

});
