<?php

$tester->global_state['tests_path'] = WP_CONTENT_DIR.'/tests';

$tester->get_tests_path = function() use ($tester) {
  return $tester->global_state['tests_path'];
};

$tester->set_tests_path = function($path) use ($tester) {
  $tester->global_state['tests_path'] = $path;
};

$tester->load_test_path = function($part = 'index', $route = '') use ($tester) {

  global $wp;

  $tests_path = $tester->get_tests_path();

  $file_path = '';

  switch ($part) {

    case 'head': $file_path = "{$tests_path}/head.php"; break;
    case 'header': $file_path = "{$tests_path}/header.php"; break;
    case 'footer': $file_path = "{$tests_path}/footer.php"; break;
    case 'index': $file_path = "{$tests_path}/{$route}".(empty($route) ? '' : '/')."index.php"; break;

    default: return false;
  }

  if (!file_exists($file_path)) return false;

  // Test file has $tester in its scope

  include $file_path;

  return true;
};
